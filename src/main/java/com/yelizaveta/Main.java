package com.yelizaveta;

import com.yelizaveta.service.RoleHandler;
import com.yelizaveta.util.Authorization;
import com.yelizaveta.util.SessionFactory;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    private void run() {

        try (Scanner in = new Scanner(System.in)) {
            Authorization authorization = new Authorization();
            authorization.authorize(in);
            SessionFactory factory = new SessionFactory();
            factory.getSession(RoleHandler.role).start(in);
        }
    }
}
