package com.yelizaveta.controller;

import com.yelizaveta.dao.UserDao;
import com.yelizaveta.model.account.Role;
import com.yelizaveta.model.account.User;
import com.yelizaveta.service.RoleHandler;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AccountController {

    private UserDao userDao;

    public boolean register(User user) {
        if (userDao.getByEmail(user.getEmail()) == null) {
            userDao.save(user);
            RoleHandler.role = Role.USER;
            return true;
        } else {
            System.out.println("Email already exists or bad password");
            return false;
        }
    }

    public boolean login(String email, String password) {
        User user = userDao.getByEmail(email);
        System.out.println(user);
        if (user != null) {
            RoleHandler.role = user.getRole();
            System.out.println(password + user.getPassword());
            return user.getPassword().equals(password);
        }
        System.out.println("Wrong email");
        return false;
    }
}
