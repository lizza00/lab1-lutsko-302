package com.yelizaveta.controller;


import com.yelizaveta.dao.CountryDao;
import com.yelizaveta.dao.TourDao;
import com.yelizaveta.model.Country;
import com.yelizaveta.model.Tour;

public class AdminTourController extends TourController {
    private TourDao tourDao;

    private CountryDao countryDao;

    public boolean addCountry(Country country) {
        if (countryDao.getByName(country.getName()) == null) {
            countryDao.save(country);
            return true;
        } else {
            System.out.println("Country with this name already exists");
            return false;
        }
    }

    public boolean addTour(Tour tour) {
        if (tourDao.getByName(tour.getName()) == null || tourDao.getByName(tour.getName()).size() == 0) {
            tourDao.save(tour);
            return true;
        } else {
            System.out.println("Tour with this name already exists");
            return false;
        }
    }

    public boolean deleteTour(Tour tour) {
        try {
            tourDao.delete(tour);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean updateTour(Tour tour) {
        try {
            tourDao.update(tour);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

