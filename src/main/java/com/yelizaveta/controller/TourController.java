package com.yelizaveta.controller;

import com.yelizaveta.dao.CountryDao;
import com.yelizaveta.dao.TourDao;
import com.yelizaveta.model.Country;
import com.yelizaveta.model.Tour;
import lombok.AllArgsConstructor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TourController {
    private TourDao tourDao = new TourDao();

    private CountryDao countryDao = new CountryDao();

    public Set<Tour> getTours(String pattern) {
        Set<Tour> result = new HashSet<>();
        result.addAll(tourDao.getByName(pattern));
        result.addAll(tourDao.getByCountry(pattern));
        return result;
    }

    public List<Country> getCountries() {
        return countryDao.getAll();
    }
}
