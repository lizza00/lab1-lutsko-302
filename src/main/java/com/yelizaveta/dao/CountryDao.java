package com.yelizaveta.dao;

import com.yelizaveta.model.Country;
import com.yelizaveta.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CountryDao {
    private Session session = HibernateUtil.getHibernateSession();


    public void save(Country country) {
        Transaction transaction = session.beginTransaction();
        session.save(country);
        transaction.commit();
    }


    public List<Country> getAll() {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Country> criteriaQuery = criteriaBuilder.createQuery(Country.class);
        Root<Country> root = criteriaQuery.from(Country.class);
        criteriaQuery.select(root);
        return session.createQuery(criteriaQuery).getResultList();
    }

    public Country getByName(String name) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Country> criteriaQuery = criteriaBuilder.createQuery(Country.class);
        Root<Country> root = criteriaQuery.from(Country.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("name"), name));
        return session.createQuery(criteriaQuery).getResultList().stream().findFirst().orElse(null);
    }

}
