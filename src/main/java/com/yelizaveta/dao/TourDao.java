package com.yelizaveta.dao;

import com.yelizaveta.model.Tour;
import com.yelizaveta.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class TourDao {

    private Session session = HibernateUtil.getHibernateSession();

    public void save(Tour tour) {
        Transaction transaction = session.beginTransaction();
        session.save(tour);
        transaction.commit();
    }

    public List<Tour> getByName(String name) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Tour> criteriaQuery = criteriaBuilder.createQuery(Tour.class);
        Root<Tour> root = criteriaQuery.from(Tour.class);
        criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), name));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public List<Tour> getByCountry(String name) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Tour> criteriaQuery = criteriaBuilder.createQuery(Tour.class);
        Root<Tour> root = criteriaQuery.from(Tour.class);
        criteriaQuery.select(root).where(criteriaBuilder.like(root.get("country"), name));
        return session.createQuery(criteriaQuery).getResultList();
    }

    public List<Tour> getAll() {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Tour> criteriaQuery = criteriaBuilder.createQuery(Tour.class);
        Root<Tour> root = criteriaQuery.from(Tour.class);
        criteriaQuery.select(root);
        return session.createQuery(criteriaQuery).getResultList();
    }

    public void update(Tour tour) {
        Transaction transaction = session.beginTransaction();
        session.update(tour);
        transaction.commit();
    }

    public void delete(Tour tour) {
        Transaction transaction = session.beginTransaction();
        session.delete(tour);
        transaction.commit();
    }
}
