package com.yelizaveta.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "tour")
public class Tour {
    @Id
    @GeneratedValue
    @Column(name = "tour_id")
    private int id;

    @Column(name = "tour_name")
    private String name;

    @Column(name = "tour_cost")
    private double cost;

    @Column(name = "tour_duration")
    private int duration;

    @Column(name = "tour_description")
    private String description;

    @ManyToOne(targetEntity = Country.class)
    @JoinColumn(referencedColumnName = "country_id", name = "tour_country_id")
    private Country country;

}
