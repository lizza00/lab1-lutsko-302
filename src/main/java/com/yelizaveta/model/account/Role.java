package com.yelizaveta.model.account;


public enum Role {
    USER,
    ADMIN
}
