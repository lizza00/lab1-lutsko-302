package com.yelizaveta.util;

import com.yelizaveta.controller.AdminTourController;
import com.yelizaveta.model.Country;
import com.yelizaveta.model.Tour;

import java.util.Scanner;

public class AdminSession implements Session {
    @Override
    public void start(Scanner in) {
        try {
            while (true) {
                System.out.println("See countries-1\n" +
                        "Add  country-2\n" +
                        "See tours-3\n" +
                        "Add tour-4\n" +
                        "Delete tour-5\n" +
                        "Update tour-6\n");

                int i = Integer.parseInt(in.nextLine());
                AdminTourController controller = new AdminTourController();

                String name;
                switch (i) {
                    case 1:
                        System.out.println(controller.getCountries());
                        break;
                    case 2:
                        System.out.println("Enter name:");
                        name = in.nextLine();
                        System.out.println("Is visa required? boolean");
                        boolean visa = in.nextBoolean();
                        controller.addCountry(Country.builder()
                                .name(name)
                                .visaRequired(visa)
                                .build());
                        break;
                    case 3:
                        System.out.println("Enter word:");
                        System.out.println(controller.getTours(in.nextLine()));
                        break;
                    case 4:
                        System.out.println("Enter name:");
                        name = in.nextLine();
                        System.out.println("Enter cost:");
                        double cost = Double.parseDouble(in.nextLine());
                        System.out.println("Enter duration:");
                        int duration = Integer.parseInt(in.nextLine());

                        System.out.println("Enter description:");
                        String description = in.nextLine();

                        System.out.println("Enter country id:");
                        System.out.println(controller.getCountries());
                        int id = Integer.parseInt(in.nextLine());
                        controller.addTour(Tour.builder()
                                .name(name)
                                .cost(cost)
                                .duration(duration)
                                .description(description)
                                .country(Country.builder()
                                        .id(id)
                                        .build())
                                .build());
                        break;
                    case 5:
                        System.out.println("Enter tour id:");
                        controller.getTours("");
                        int tourId = Integer.parseInt(in.nextLine());
                        controller.deleteTour(Tour.builder()
                                .id(tourId)
                                .build());
                        break;
                    case 6:
                        System.out.println("Enter tour id:");
                        controller.getTours("");
                        int tourIdU = Integer.parseInt(in.nextLine());
                        System.out.println("Enter name:");
                        name = in.nextLine();
                        System.out.println("Enter cost:");
                        double costU = Double.parseDouble(in.nextLine());
                        System.out.println("Enter duration:");
                        int durationU = Integer.parseInt(in.nextLine());

                        System.out.println("Enter description:");
                        String descriptionU = in.nextLine();

                        System.out.println("Enter country id:");
                        System.out.println(controller.getCountries());
                        int idU = Integer.parseInt(in.nextLine());
                        controller.updateTour(Tour.builder()
                                .id(tourIdU)
                                .name(name)
                                .cost(costU)
                                .duration(durationU)
                                .description(descriptionU)
                                .country(Country.builder()
                                        .id(idU)
                                        .build())
                                .build());
                }

            }
        } catch (Exception e) {
            start(in);
        }
    }
}
