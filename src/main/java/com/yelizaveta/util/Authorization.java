package com.yelizaveta.util;

import com.yelizaveta.controller.AccountController;
import com.yelizaveta.dao.UserDao;
import com.yelizaveta.model.account.Role;
import com.yelizaveta.model.account.User;

import java.util.Scanner;

public class Authorization {

    public void authorize(Scanner in) {
        try {
            System.out.println("Press 1 to login\nPress 2 to register");
            int i = Integer.parseInt(in.nextLine());
            AccountController controller = new AccountController(new UserDao());
            if (i == 1) {
                System.out.println("Enter email:");
                String email = in.nextLine();
                System.out.println("Enter password:");
                String password = in.nextLine();
                if (!controller.login(email, password)) {
                    throw new Exception();
                }
            } else if (i == 2) {
                System.out.println("Enter email:");
                String email = in.nextLine();
                System.out.println("Enter password:");
                String password = in.nextLine();
                System.out.println("Enter name:");
                String name = in.nextLine();
                if (!controller.register(User.builder()
                        .email(email)
                        .password(password)
                        .name(name)
                        .role(Role.USER)
                        .build())) {
                    throw new Exception();
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
            authorize(in);
        }
    }

}
