package com.yelizaveta.util;

import java.util.Scanner;

public interface Session {
    void start(Scanner in);
}
