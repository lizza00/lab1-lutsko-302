package com.yelizaveta.util;

import com.yelizaveta.model.account.Role;

public class SessionFactory {

    public Session getSession(Role role){
        if(role.equals(Role.ADMIN)){
            return new AdminSession();
        }
        else{
            return new UserSession();
        }
    }
}
