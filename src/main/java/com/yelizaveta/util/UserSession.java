package com.yelizaveta.util;

import com.yelizaveta.controller.TourController;

import java.util.Scanner;

public class UserSession implements Session {
    @Override
    public void start(Scanner in) {
        try {
            while (true) {
                System.out.println("See countries-1\nSee tours-2");

                int i = Integer.parseInt(in.nextLine());
                TourController controller = new TourController();
                if (i == 1) {
                    System.out.println(controller.getCountries());
                } else if (i == 2) {
                    System.out.println("Enter word:");
                    System.out.println(controller.getTours(in.nextLine()));
                }
            }
        } catch (Exception e) {
            start(in);
        }
    }
}
